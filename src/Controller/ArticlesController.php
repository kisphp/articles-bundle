<?php

namespace Kisphp\ArticlesBundle\Controller;

use Kisphp\ArticlesBundle\Form\ArticleForm;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template()
 */
class ArticlesController extends AbstractController
{
    const SECTION_TITLE = 'section.title.articles';
    const MODEL_NAME = 'model.article';
    const ENTITY_FORM_CLASS = ArticleForm::class;
    const LISTING_TEMPLATE = '@Articles/Articles/index.html.twig';
    const EDIT_TEMPLATE = '@Articles/Articles/edit.html.twig';

    const HTML_EDITOR_ENABLED = true;
    const UPLOAD_TYPE = 'article';

    const ALLOW_SEARCH = true;

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_articles_edit',
        self::LIST_PATH => 'adm_articles',
        self::ADD_PATH => 'adm_articles_add',
        self::STATUS_PATH => 'adm_articles_status',
        self::DELETE_PATH => 'adm_articles_delete',
    ];

    /**
     * @return string
     *
     * @param mixed $id
     */
    protected function getImagesEditUrl($id)
    {
        return $this->generateUrl('adm_articles_attached', [
            'id' => $id,
        ]);
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $model = $this->get('model.category');
        $form = $this->createForm(static::ENTITY_FORM_CLASS, $object, [
            'categories_choices' => $model->getCategoriesForForm(),
            'category_model' => $model,
        ]);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param int $articleId
     *
     * @return array
     *
     * @throws \UnexpectedValueException
     */
    protected function getAttachedFiles($articleId)
    {
        $model = $this->get('model.media_files');

        return $model->findBy([
            'id_object' => $articleId,
            'object_type' => static::UPLOAD_TYPE,
        ]);
    }
}
