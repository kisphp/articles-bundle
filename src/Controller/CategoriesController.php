<?php

namespace Kisphp\ArticlesBundle\Controller;

use Kisphp\ArticlesBundle\Form\CategoryForm;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template()
 */
class CategoriesController extends AbstractController
{
    const SECTION_TITLE = 'section.title.categories';
    const MODEL_NAME = 'model.category';
    const ENTITY_FORM_CLASS = CategoryForm::class;
    const LISTING_TEMPLATE = '@Articles/Categories/index.html.twig';
    const EDIT_TEMPLATE = '@Articles/Categories/edit.html.twig';

    const HTML_EDITOR_ENABLED = true;
    const UPLOAD_TYPE = 'category';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'adm_article_categories_edit',
        self::LIST_PATH => 'adm_article_categories',
        self::ADD_PATH => 'adm_article_categories_add',
        self::STATUS_PATH => 'adm_article_categories_status',
        self::DELETE_PATH => 'adm_article_categories_delete',
    ];

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $items = $this->get(static::MODEL_NAME)
            ->getCategoriesForListing()
        ;

        return $this->render(
            static::LISTING_TEMPLATE,
            [
                'section_title' => static::SECTION_TITLE . '.list',
                'items' => $items,
                'section' => $this->section,
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id category id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function attachedBackgroundAction(Request $request, $id)
    {
        /** @var \Kisphp\ArticlesBundle\Entity\Category $entity */
        $entity = $this->get('model.category')->find($id);
        $attached = $this->getAttachedFiles($id);

        return $this->render('@Articles/Categories/background.html.twig', [
            'entity' => $entity,
            'attached' => $attached,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function attachedBackgroundRemoveAction(Request $request)
    {
        $categoryId = $request->request->get('id');

        /** @var \Kisphp\ArticlesBundle\Entity\Category $entity */
        $entity = $this->get('model.category')->find($categoryId);

        if ($entity === null) {
            return new JsonResponse([
                'code' => 404,
            ]);
        }

        $entity->removeBackgroundImage();

        $this->get('model.category')->save($entity);

        return new JsonResponse([
            'code' => 200,
            'id_category' => $entity->getId(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function attachedBackgroundSetAction(Request $request)
    {
        $imageId = $request->request->get('id');
        $categoryId = $request->request->get('cat');

        /** @var \Kisphp\ArticlesBundle\Entity\Category $entity */
        $entity = $this->get('model.category')->find($categoryId);

        /** @var \Kisphp\MediaBundle\Entity\MediaFile $img */
        $img = $this->get('model.media_files')->setType('category')->find($imageId);

        if ($img === null || $entity === null) {
            return new JsonResponse([
                'code' => 404,
            ]);
        }

        $entity->setBackgroundImage($img);

        $this->get('model.category')->save($img);

        return new JsonResponse([
            'code' => 200,
            'id_category' => $entity->getId(),
            'id_image' => $img->getId(),
        ]);
    }

    /**
     * @param Request $request
     * @param KisphpEntityInterface $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $form = $this->createForm(static::ENTITY_FORM_CLASS, $object, [
            'categories_choices' => $this->get('model.category')->getCategoriesForForm(),
        ]);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @return string
     *
     * @param mixed $id
     */
    protected function getImagesEditUrl($id)
    {
        return $this->generateUrl('adm_article_categories_attached', [
            'id' => $id,
        ]);
    }

    /**
     * @param int $articleId
     *
     * @return array
     */
    protected function getAttachedFiles($articleId)
    {
        $model = $this->get('model.media_files');

        return $model->findBy([
            'id_object' => $articleId,
            'object_type' => static::UPLOAD_TYPE,
        ]);
    }
}
