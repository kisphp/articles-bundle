<?php

namespace Kisphp\ArticlesBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItemInterface;

class ArticlesMenuItems implements MenuItemInterface
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => true,
            'valid_feature' => 'articles',
            'label' => 'Main Menu',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'articles',
            'match_route' => 'article_categories',
            'path' => 'adm_article_categories',
            'icon' => 'fa-sitemap',
            'label' => 'main_navigation.categories',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'articles',
            'match_route' => 'articles',
            'path' => 'adm_articles',
            'icon' => 'fa-file',
            'label' => 'main_navigation.articles',
        ]);
    }
}
