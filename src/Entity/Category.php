<?php

namespace Kisphp\ArticlesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\FileInterface;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;
use Kisphp\Utils\Strings;

/**
 * @ORM\Table(name="categories", options={"colate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="Kisphp\ArticlesBundle\Repository\CategoryRepository")
 */
class Category implements KisphpEntityInterface, ToggleableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true, "default": 0})
     */
    protected $id_parent = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::ACTIVE;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category")
     */
    protected $articles;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $item_url;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $seo_title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $seo_keywords;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $seo_description;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $body;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $background_image;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdParent()
    {
        return $this->id_parent;
    }

    /**
     * @param int $id_parent
     */
    public function setIdParent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setItemUrl($title);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getItemUrl()
    {
        return $this->item_url;
    }

    /**
     * @param string $item_url
     */
    public function setItemUrl($item_url)
    {
        $this->item_url = Strings::niceUrlTitle($item_url);
    }

    /**
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * @param string $seo_title
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;
    }

    /**
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * @param string $seo_keywords
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;
    }

    /**
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * @param string $seo_description
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;
    }

    /**
     * @return array
     */
    public function getBackgroundImage()
    {
        return json_decode($this->background_image, true);
    }

    /**
     * @param FileInterface $image
     */
    public function setBackgroundImage(FileInterface $image)
    {
        $this->background_image = json_encode([
            'id' => $image->getId(),
            'filename' => $image->getFilename(),
            'directory' => $image->getDirectory(),
        ]);
    }

    public function removeBackgroundImage()
    {
        $this->background_image = null;
    }
}
