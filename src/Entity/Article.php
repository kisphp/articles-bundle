<?php

namespace Kisphp\ArticlesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;
use Kisphp\Utils\Strings;

/**
 * @ORM\Table(name="articles", options={"colate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="Kisphp\ArticlesBundle\Repository\ArticleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Article implements KisphpEntityInterface, ToggleableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_category;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $is_tutorial = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $article_url;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registered;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     * @ORM\JoinColumn(name="id_category", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::ACTIVE;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seo_title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seo_keywords;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seo_description;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $body;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDatetime()
    {
        if ($this->registered === null) {
            $this->setRegistered(new \DateTime());
        }
    }

    /**
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * @param \DateTime $registered
     */
    public function setRegistered(\DateTime $registered)
    {
        $this->registered = $registered;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setArticleUrl($title);
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdCategory()
    {
        return $this->id_category;
    }

    /**
     * @param int $id_category
     */
    public function setIdCategory($id_category)
    {
        $this->id_category = $id_category;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
        if ($category->getId() > 0) {
            $this->id_category = $category->getId();
        }
    }

    /**
     * @return string
     */
    public function getArticleUrl()
    {
        return $this->article_url;
    }

    /**
     * @param string $article_url
     */
    public function setArticleUrl($article_url)
    {
        $this->article_url = Strings::niceUrlTitle($article_url);
    }

    /**
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * @param string $seo_title
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;
    }

    /**
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * @param string $seo_keywords
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;
    }

    /**
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * @param string $seo_description
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;
    }

    /**
     * @return bool
     */
    public function isTutorial()
    {
        return (bool) $this->is_tutorial;
    }

    /**
     * @param bool $is_tutorial
     */
    public function setIsTutorial($is_tutorial)
    {
        $this->is_tutorial = (bool) $is_tutorial;
    }
}
