<?php

namespace Kisphp\ArticlesBundle\Form;

use Kisphp\ArticlesBundle\Entity\Article;
use Kisphp\Utils\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArticleForm extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => Article::class,
        ]);

        $resolver->setRequired('categories_choices');
        $resolver->setRequired('category_model');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('id_category', ChoiceType::class, [
            'choices' => $options['categories_choices'],
            'choice_label' => function($choice, $key, $value){
                $data = explode('|', $key);
                return end($data);
            },
        ]);
        $builder->add('status', ChoiceType::class, [
            'expanded' => true,
            'choices' => [
                'status.active' => Status::ACTIVE,
                'status.inactive' => Status::INACTIVE,
            ],
            'attr' => [
                'class' => 'input-choice',
            ],
        ]);

        $builder->add('is_tutorial', CheckboxType::class, [
            'required' => false,
        ]);
        $builder->add('body', TextareaType::class, [
            'attr' => $this->getBodyFieldClassName($options['data']),
            'constraints' => [
                new NotBlank(),
            ],
        ]);

        $builder->add('seo_title', TextType::class);
        $builder->add('seo_keywords');
        $builder->add('seo_description', TextareaType::class, [
            'constraints' => [
                new Length([
                    'max' => 255,
                    'maxMessage' => 'string.max.maxMessage',
                ]),
            ],
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                return $this->transformDbToEntity($entity);
            },
            function ($entity) use ($options) {
                return $this->transformEntityToDb($entity, $options);
            }
        ));
    }

    /**
     * @param \Kisphp\ArticlesBundle\Entity\Article $article
     * @param array $options
     *
     * @return \Kisphp\ArticlesBundle\Entity\Article
     */
    protected function transformDbToEntity(Article $article)
    {
        return $article;
    }

    /**
     * @param Article $entity
     * @param array $options
     *
     * @return Article
     */
    protected function transformEntityToDb(Article $entity, array $options)
    {
        /** @var \Kisphp\ArticlesBundle\Model\CategoryModel $model */
        $model = $options['category_model'];

        /** @var \Kisphp\ArticlesBundle\Entity\Category $categ */
        $categ = $model->find($entity->getIdCategory());
        $entity->setCategory($categ);

        $entity->setArticleUrl($entity->getTitle());

        if (empty($entity->getSeoTitle())) {
            $entity->setSeoTitle($entity->getTitle());
        }

        return $entity;
    }

    /**
     * @param Article $article
     *
     * @return array
     */
    protected function getBodyFieldClassName(Article $article)
    {
        if ($article->isTutorial()) {
            return [
                'rows' => 24,
            ];
        }

        return [
            'class' => 'html-edit',
            'rows' => 7,
        ];
    }
}
