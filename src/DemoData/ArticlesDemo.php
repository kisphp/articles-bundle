<?php

namespace Kisphp\ArticlesBundle\DemoData;

use Kisphp\Faker\Factory;
use Kisphp\FrameworkAdminBundle\Fixtures\AbstractDemoData;
use Kisphp\Utils\Strings;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ArticlesDemo extends AbstractDemoData
{
    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function loadDemoData(InputInterface $input, OutputInterface $output)
    {
        $this->createCategories($output);
        $this->createArticle($output);
        $this->createServices($output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function createArticle(OutputInterface $output)
    {
        $faker = Factory::create();
        $model = $this->getContainer()->get('model.article');

        $cat = $this->getContainer()->get('model.category');

        /** @var \Kisphp\ArticlesBundle\Entity\Category $category */
        $category = $cat->findOneBy([
            'item_url' => 'category-1',
        ]);

        /** @var \Kisphp\ArticlesBundle\Entity\Article $art */
        $art = $model->createEntity();
        $art->setTitle('My Article');
        $art->setBody($faker->text->randomParagraph());
        $art->setIsTutorial(true);
        $art->setCategory($category);

        $model->persist($art);

        $output->writeln(sprintf('Created article in articles category <info>%s</info>', $art->getTitle()));

        $model->flush();
    }

    /**
     * @param OutputInterface $output
     */
    protected function createCategories(OutputInterface $output)
    {
        $model = $this->getContainer()->get('model.category');

        // 1
        $cat = $model->createCategoryEntity('category 1');
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 2
        $cat = $model->createCategoryEntity('category 2');
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 3
        $cat = $model->createCategoryEntity('category 3');
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 4
        $cat = $model->createCategoryEntity('category 4');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 5
        $cat = $model->createCategoryEntity('category 5');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 6
        $cat = $model->createCategoryEntity('category 6');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 7
        $cat = $model->createCategoryEntity('category 7');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 8
        $cat = $model->createCategoryEntity('category 8');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 9
        $cat = $model->createCategoryEntity('category 9');
        $cat->setIdParent(8);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 10
        $cat = $model->createCategoryEntity('category 10');
        $cat->setIdParent(8);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 11
        $cat = $model->createCategoryEntity('category 11');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 12
        $cat = $model->createCategoryEntity('category 12');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 13
        $cat = $model->createCategoryEntity('category 13');
        $cat->setIdParent(3);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 14
        $cat = $model->createCategoryEntity('category 14');
        $cat->setIdParent(0);
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        $model->flush();
        $output->writeln('<info>Created categories</info>');
    }

    /**
     * @param OutputInterface $output
     */
    protected function createServices(OutputInterface $output)
    {
        $faker = Factory::create();
        $model = $this->getContainer()->get('model.article');

        $cat = $this->getContainer()->get('model.category');

        $categories = $cat->findAll();

        $articles = [];
        for ($i = 1; $i <= 30; ++$i) {
            /** @var \Kisphp\ArticlesBundle\Entity\Article $art */
            $art = $model->createEntity();
            $art->setTitle($faker->text(30));
            $art->setBody($faker->text->randomParagraph());
            $art->setCategory($categories[mt_rand(3, 12)]);

            $articles[$i] = $art;

            $model->persist($articles[$i]);

            $output->writeln(sprintf('Created article <info>%s</info>', $art->getTitle()));
        }

        $model->flush();
    }
}
