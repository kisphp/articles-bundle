<?php

namespace Kisphp\ArticlesBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\Utils\Status;

class CategoryRepository extends EntityRepository
{
    /**
     * @param int $parentId
     *
     * @return array
     */
    public function getCategoriesByParentId($parentId, array $allowedStatuses)
    {
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.id_parent = :parent')
            ->setParameter('parent', (int) $parentId)
            ->andWhere('c.status IN (:status)')
            ->setParameter('status', $allowedStatuses)
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getActiveServices()
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.status = :status')
            ->setParameter('status', Status::ACTIVE)
            ->andWhere('c.id_parent = 3')
        ;

        return $query->getQuery()->getResult();
    }
}
