<?php

namespace Kisphp\ArticlesBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\ArticlesBundle\Entity\Category;
use Kisphp\Utils\Status;

class ArticleRepository extends EntityRepository
{
    /**
     * @return \Kisphp\ArticlesBundle\Entity\Article[]
     */
    public function getActiveArticles()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status = :status')
            ->setParameter('status', Status::ACTIVE)
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Category $category
     *
     * @return \Kisphp\ArticlesBundle\Entity\Article[]
     */
    public function getArticlesByCategory(Category $category)
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.id_category = :categ')
            ->setParameter('categ', $category->getId())
        ;

        return $query->getQuery()->getResult();
    }
}
