<?php

namespace Kisphp\ArticlesBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\ArticlesBundle\Entity\Article;
use Kisphp\ArticlesBundle\Entity\Category;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;
use Kisphp\MediaBundle\Model\MediaModelInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method ArticleRepository|EntityRepository getRepository()
 */
class ArticleModel extends AbstractModel implements MediaModelInterface
{
    const REPOSITORY = 'ArticlesBundle:Article';

    /**
     * @return string
     */
    public function getMediaType()
    {
        return 'article';
    }

    /**
     * @param Category $category
     *
     * @return Article[]
     */
    public function getArticlesByCategory(Category $category)
    {
        return $this->getRepository()->getArticlesByCategory($category);
    }

    /**
     * @return KisphpEntityInterface
     */
    public function createEntity()
    {
        return new Article();
    }

    /**
     * @return Article[]
     */
    public function getActiveArticles()
    {
        return $this->getRepository()->getActiveArticles();
    }

    /**
     * @param Article $article
     *
     * @return array
     */
    public function toArray(Article $article)
    {
        return [
            'id' => $article->getId(),
            'body' => $article->getBody(),
            'status' => $article->getStatus(),
            'title' => $article->getTitle(),
            'category' => [
                'id' => $article->getCategory()->getId(),
                'title' => $article->getCategory()->getTitle(),
                'status' => $article->getCategory()->getStatus(),
                'seo_title' => $article->getCategory()->getSeoTitle(),
            ],
        ];
    }

    /**
     * @param null|\Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getItemsQuery(Request $request = null)
    {
        $categoryId = $request->query->get('c');

        $query = parent::getItemsQuery($request);

        if ($categoryId > 0) {
            $query->andWhere('a.id_category = :categ');
            $query->setParameter('categ', $categoryId);
        }

        return $query;
    }
}
