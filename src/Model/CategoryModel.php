<?php

namespace Kisphp\ArticlesBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\ArticlesBundle\Entity\Category;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;
use Kisphp\MediaBundle\Model\MediaModelInterface;
use Kisphp\Utils\Status;

/**
 * @method EntityRepository|Kisphp\ArticlesBundle\Repository\CategoryRepository getRepository()
 */
class CategoryModel extends AbstractModel implements MediaModelInterface
{
    const REPOSITORY = 'ArticlesBundle:Category';

    /**
     * @return string
     */
    public function getMediaType()
    {
        return 'category';
    }

    /**
     * @return \Kisphp\ArticlesBundle\Entity\Category
     *
     * @param null|mixed $categoryTitle
     */
    public function createCategoryEntity($categoryTitle = null)
    {
        /** @var \Kisphp\ArticlesBundle\Entity\Category $cat */
        $cat = $this->createEntity();

        if ($categoryTitle !== null) {
            $cat->setTitle($categoryTitle);
        }

        return $cat;
    }

    /**
     * @return array
     */
    public function getActiveServices()
    {
        return $this->getRepository()->getActiveServices();
    }

    /**
     * @return array
     */
    public function getCategoriesForForm()
    {
        $level = 0;
        $categories = [];
        $this->getCategoriesChildren($categories, 0, $level);

        return $categories;
    }

    /**
     * @return array
     */
    public function getCategoriesForListing()
    {
        $level = 0;
        $categories = [];
        $this->getCategoryEntitiesChildren($categories, 0, $level);

        return $categories;
    }

    /**
     * @param \Kisphp\ArticlesBundle\Entity\Category $category
     *
     * @return array
     */
    public function toArrayForMenu(Category $category)
    {
        return [
            'id' => $category->getId(),
            'title' => $category->getTitle(),
            'status' => $category->getStatus(),
            'item_url' => $category->getItemUrl(),
        ];
    }

    /**
     * @param Category $category
     *
     * @return array
     */
    public function toArray(Category $category)
    {
        return [
            'id' => $category->getId(),
            'title' => $category->getTitle(),
            'status' => $category->getStatus(),
            'item_url' => $category->getItemUrl(),
            'body' => $category->getBody(),
            'seo_title' => $category->getSeoTitle(),
            'seo_keywords' => $category->getSeoKeywords(),
            'seo_description' => $category->getSeoDescription(),
            'id_parent' => $category->getIdParent(),
        ];
    }

    /**
     * @return Category
     */
    public function createEntity()
    {
        return new Category();
    }

    /**
     * @param array $categories
     * @param int $parentId
     * @param mixed $level
     */
    protected function getCategoriesChildren(array &$categories, $parentId, $level)
    {
        $list = $this->getRepository()
            ->getCategoriesByParentId(
                $parentId,
                [
                    Status::ACTIVE,
                    Status::INACTIVE,
                ]
            );
        /** @var Category $cat */
        foreach ($list as $cat) {
            $key = '';
            if ($level > 0) {
                $key .= str_repeat('-', $level) . ' ';
            }
            $key .= $cat->getTitle();

            $categories[$cat->getId() . '|' . $key] = $cat->getId();
            $this->getCategoriesChildren($categories, $cat->getId(), $level + 1);
        }
    }

    /**
     * @param array $categories
     * @param int $parentId
     * @param mixed $level
     */
    protected function getCategoryEntitiesChildren(array &$categories, $parentId, $level)
    {
        $list = $this->getRepository()
            ->getCategoriesByParentId(
                $parentId,
                [
                Status::ACTIVE,
                Status::INACTIVE,
            ]
        );
        /** @var Category $cat */
        foreach ($list as $cat) {
            $categories[$cat->getId()] = [
                'level' => $level,
                'object' => $cat,
            ];
            $this->getCategoryEntitiesChildren($categories, $cat->getId(), $level + 1);
        }
    }
}
