<?php

namespace Functional\ArticlesBundle\Controller;

use Codeception\Example;

/**
 * @group status
 */
class StatusRequestCest
{
    /**
     * @param \FunctionalTester $i
     *
     * @dataProvider statusUrlsProvider
     */
    public function status_change(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    public function statusUrlsProvider()
    {
        return [
            ['url' => '/articles/status'],
            ['url' => '/categories/status'],
        ];
    }
}
