<?php

namespace Functional\ArticlesBundle\Controller;

/**
 * @group articles
 */
class ArticlesControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_page(\FunctionalTester $i)
    {
        $i->amOnPage('/articles');
        $i->see('Articles', 'h3');
        $i->see('Title');
        $i->see('Status');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function listing_page_with_specific_category(\FunctionalTester $i)
    {
        $i->amOnPage('/articles?c=1');
        $i->see('Articles', 'h3');
        $i->see('Title');
        $i->see('Status');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function listing_page_with_search(\FunctionalTester $i)
    {
        $i->amOnPage('/articles?q=animi');
        $i->see('Articles', 'h3');
        $i->see('Title');
        $i->see('Status');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_page(\FunctionalTester $i)
    {
        $i->amOnPage('/articles');
        $i->see('Edit');
        $i->click('Edit');
        $i->see('Edit Article');
        $i->see('View Article');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page(\FunctionalTester $i)
    {
        $i->amOnPage('/articles');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Article');
        $i->cantSee('View Article');
        $i->fillField('article_form[title]', 'Article title');
        $i->fillField('article_form[body]', 'Article content');
        $i->click('Save');
        $i->canSeeCurrentUrlMatches('/articles\/edit/');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page_no_filling(\FunctionalTester $i)
    {
        $i->amOnPage('/articles');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Article');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('This value should not be blank');
    }
}
