<?php

namespace Functional\ArticlesBundle\Controller;

/**
 * @group categories
 */
class CategoriesControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_page(\FunctionalTester $i)
    {
        $i->amOnPage('/categories');
        $i->see('Categories', 'h3');
        $i->see('Title');
        $i->see('Status');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_page(\FunctionalTester $i)
    {
        $i->amOnPage('/categories');
        $i->see('Edit');
        $i->click('Edit');
        $i->see('Edit Category');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page(\FunctionalTester $i)
    {
        $i->amOnPage('/categories');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Category');
        $i->fillField('category_form[title]', 'Category title');
        $i->fillField('category_form[body]', 'Category content');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/categories\/edit/');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page_no_filling(\FunctionalTester $i)
    {
        $i->amOnPage('/categories');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Category');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('This value should not be blank');
    }
}
