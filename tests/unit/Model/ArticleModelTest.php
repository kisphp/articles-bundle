<?php

namespace Unit\ArticlesBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Kisphp\ArticlesBundle\Entity\Article;
use Kisphp\ArticlesBundle\Entity\Category;
use Kisphp\ArticlesBundle\Model\ArticleModel;
use Kisphp\ArticlesBundle\Repository\ArticleRepository;
use Kisphp\Utils\Status;
use PHPUnit\Framework\TestCase;

/**
 * @group articles
 */
class ArticleModelTest extends TestCase
{
    public function test_get_type()
    {
        $model = $this->createModel();

        $this->assertSame('article', $model->getMediaType());
    }

    public function test_getArticlesByCategory()
    {
        $model = $this->createModel();

        $category = new Category();
        $category->setId(1);

        $articles = $model->getArticlesByCategory($category);

        $this->assertInstanceOf(Article::class, $articles[0]);
    }

    public function test_get_active_articles()
    {
        $model = $this->createModel();

        $articles = $model->getActiveArticles();

        $this->assertInstanceOf(Article::class, $articles[0]);
    }

    public function test_toArray()
    {
        $model = $this->createModel();

        $category = new Category();
        $category->setId(1);
        $category->setTitle('Category');

        $article = new Article();
        $article->setId(1);
        $article->setCategory($category);
        $article->setTitle('Article');
        $article->setBody('article body');

        $this->assertSame([
            'id' => 1,
            'body' => 'article body',
            'status' => Status::ACTIVE,
            'title' => 'Article',
            'category' => [
                'id' => 1,
                'title' => 'Category',
                'status' => Status::ACTIVE,
                'seo_title' => null,
            ]
        ], $model->toArray($article));
    }

    /**
     * @return \Kisphp\ArticlesBundle\Model\ArticleModel
     * @throws \Kisphp\Exceptions\ConstantNotFound
     */
    protected function createModel(): \Kisphp\ArticlesBundle\Model\ArticleModel
    {
        $qb = \Mockery::mock(QueryBuilder::class);
        $qb->shouldReceive('select')->andReturnSelf();
        $qb->shouldReceive('from')->andReturnSelf();
        $qb->shouldReceive('where')->andReturnSelf();
        $qb->shouldReceive('andWhere')->andReturnSelf();
        $qb->shouldReceive('setParameter')->andReturnSelf();
        $qb->shouldReceive('getQuery')->andReturnSelf();
        $qb->shouldReceive('getResult')->andReturn($this->getResults());

        $em = \Mockery::mock(EntityManagerInterface::class);
        $em->shouldReceive('createQueryBuilder')->andReturn($qb);
        $em->shouldReceive('persist')->andReturnSelf();
        $em->shouldReceive('flush')->andReturnSelf();

        $metaData = \Mockery::mock(ClassMetadata::class);

        $repo = new ArticleRepository($em, $metaData);

        $em->shouldReceive('getRepository')->andReturn($repo);

        return new ArticleModel($em);
    }

    protected function getResults()
    {
        $article_1 = new Article();
        $article_1->setId(1);

        $article_2 = new Article();
        $article_2->setId(2);

        return [
            $article_1,
            $article_2,
        ];
    }
}
