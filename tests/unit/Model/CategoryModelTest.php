<?php

namespace Unit\ArticlesBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Kisphp\ArticlesBundle\Entity\Article;
use Kisphp\ArticlesBundle\Entity\Category;
use Kisphp\ArticlesBundle\Model\ArticleModel;
use Kisphp\ArticlesBundle\Model\CategoryModel;
use Kisphp\ArticlesBundle\Repository\ArticleRepository;
use Kisphp\ArticlesBundle\Repository\CategoryRepository;
use Kisphp\Utils\Status;
use PHPUnit\Framework\TestCase;

/**
 * @group articles
 * @group ar
 */
class CategoryModelTest extends TestCase
{
    public function test_get_type()
    {
        $model = $this->createModel();

        $this->assertSame('category', $model->getMediaType());
    }

    public function test_getArticlesByCategory()
    {
        $model = $this->createModel();

        $category = new Category();
        $category->setId(1);

        $articles = $model->getActiveServices($category);

        $this->assertInstanceOf(Category::class, $articles[0]);
    }

    public function test_get_active_articles()
    {
        $model = $this->createModel();

        $articles = $model->createCategoryEntity('Category');

        $this->assertInstanceOf(Category::class, $articles);
    }

    public function test_toArray()
    {
        $model = $this->createModel();

        $category = new Category();
        $category->setId(1);
        $category->setTitle('Category');

        $this->assertSame([
            'id' => 1,
            'title' => 'Category',
            'status' => Status::ACTIVE,
            'item_url' => 'category',
            'body' => null,
            'seo_title' => null,
            'seo_keywords' => null,
            'seo_description' => null,
            'id_parent' => 0,
        ], $model->toArray($category));
    }

    public function test_toArrayForMenu()
    {
        $model = $this->createModel();

        $category = new Category();
        $category->setId(1);
        $category->setTitle('Category');

        $this->assertSame([
            'id' => 1,
            'title' => 'Category',
            'status' => Status::ACTIVE,
            'item_url' => 'category',
        ], $model->toArrayForMenu($category));
    }

    /**
     * @return \Kisphp\ArticlesBundle\Model\CategoryModel
     * @throws \Kisphp\Exceptions\ConstantNotFound
     */
    protected function createModel()
    {
        $qb = \Mockery::mock(QueryBuilder::class);
        $qb->shouldReceive('select')->andReturnSelf();
        $qb->shouldReceive('from')->andReturnSelf();
        $qb->shouldReceive('where')->andReturnSelf();
        $qb->shouldReceive('andWhere')->andReturnSelf();
        $qb->shouldReceive('setParameter')->andReturnSelf();
        $qb->shouldReceive('getQuery')->andReturnSelf();
        $qb->shouldReceive('getResult')->once()->andReturn($this->getResults());

        $em = \Mockery::mock(EntityManagerInterface::class);
        $em->shouldReceive('createQueryBuilder')->andReturn($qb);
        $em->shouldReceive('persist')->andReturnSelf();
        $em->shouldReceive('flush')->andReturnSelf();

        $metaData = \Mockery::mock(ClassMetadata::class);

        $repo = new CategoryRepository($em, $metaData);

        $em->shouldReceive('getRepository')->andReturn($repo);

        return new CategoryModel($em);
    }

    protected function getResults()
    {
        $article_1 = new Category();
        $article_1->setId(1);

        $article_2 = new Category();
        $article_2->setId(2);
        $article_2->setIdParent(1);

        return [
            $article_1,
            $article_2,
        ];
    }
}
